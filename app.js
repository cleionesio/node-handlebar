const express = require('express')
const { engine } = require('express-handlebars');

const app = express();

app.use('/public', express.static(__dirname + '/public'))

app.engine('hbs', engine({ extname: '.hbs', defaultLayout: "main"}));
app.set('view engine', 'hbs');
app.set("views", "./views");

app.get('/', (req, res) => {
    res.render('home', {
        posts: [
            {
                author: 'Janith Kasun',
                image: 'https://picsum.photos/500/500',
                comments: [
                    'This is the first comment',
                    'This is the second comment',
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec fermentum ligula. Sed vitae erat lectus.'
                ]
            },
            {
                author: 'John Doe',
                image: 'https://picsum.photos/500/500?2',
                comments: [
                ]
            }
        ]
    });
});



app.get('/form', (req, res) => {
    res.render('form');
});

app.listen(3000, () => {
    console.log('The web server has started on port 3000');
});